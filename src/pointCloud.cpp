



#include <stdio.h>      /* printf, scanf, puts, NULL */
#include <stdlib.h>     /* srand, rand */
#include <vector>
#include <string>
#include <climits>
#include <limits>


#include "./pointCloud.h"

using namespace pointCloud;

/************************************************************************************/
PointCloud::PointCloud(){
	this->cloudName = "Undefined";
	this->indexTree = new Octree<double>(TREE_SIZE);
};
/************************************************************************************/
PointCloud::~PointCloud(){
	delete this->indexTree;
};
/************************************************************************************/
PointCloud::PointCloud(int noOfPoints, std::string name){
	int x,y,z;  //changed from logn
	int maxPos = 4096;
	this->cloudName = name;
	this->indexTree = new Octree<double>(TREE_SIZE);

	//Random Initialization.
	for (int i=0; i<noOfPoints;i++){
		x=rand() % maxPos;	y=rand() % maxPos;	z=rand() % maxPos;
		////x=int(rand() % 4096);	y=int(rand() % 4096);	z=int(rand() % 4096);  //coordinates are integers. 
		 
		Point * p = new Point(x,y,z);

		this->addPoint(p);
	}
};

/************************************************************************************/
PointCloud::PointCloud(PointCloud& source){//returns a copy of the PC
	std::string cName("CopyCloud");
	this->cloudName = cName;
	this->indexTree = new Octree<double>(TREE_SIZE);

	PointSet sourcePointSets = source.getPointSet();

	for(Point* point : sourcePointSets.getPoints()) {
		Point* p = new Point(point->get_X(),point->get_Y(),point->get_Z());
		this->addPoint(p);
    }

};


/************************************************************************************/
PointCloud::PointCloud(PointCloud& source, string cName, int threshold){//Add points from the original plus a threshold
	/******  TRANSFORMATION COPY ****/
	this->cloudName = cName;
	this->indexTree = new Octree<double>(TREE_SIZE);

	PointSet sourcePointSets = source.getPointSet();

	int sign,delta = 0;
	int x,y,z;

	for(Point* point : sourcePointSets.getPoints()) {
		rand() % 2 == 0 ? sign=1 : sign=-1;

		delta = (rand() % threshold)*sign;
		if (sign == 1)	(point->get_X() + delta) < TREE_SIZE ? x = (point->get_X() + delta) : x = TREE_SIZE-1;
		else 			(point->get_X() + delta) >=0 ? x = (point->get_X() + delta) : x = 0;
		
		delta = (rand() % threshold)*sign;
		if (sign == 1)	(point->get_Y() + delta) < TREE_SIZE ? y = (point->get_Y() + delta) : y = TREE_SIZE-1;
		else 			(point->get_Y() + delta) >=0 ? y = (point->get_Y() + delta) : y = 0;

		delta = (rand() % threshold)*sign;
		if (sign == 1)	(point->get_Z() + delta) < TREE_SIZE ? z = (point->get_Z() + delta) : z = TREE_SIZE-1;
		else 			(point->get_Z() + delta) >=0 ? z = (point->get_Z() + delta) : z = 0;


		Point* p = new Point(x,y,z);
		this->addPoint(p);
    }

};



/************************************************************************************/
std::vector<PointAlignment*> PointCloud::ipcAlignment(PointCloud* target){
	//returns a vector of the alignments of the clouds. 

	double minDst;
	std::vector<PointAlignment*> alignments;

	PointSet srcPoints = this->getPointSet();
	PointSet tgtPoints = target->getPointSet();

	/*Find Near neighbor for each point in source.*/
	for (Point* pointSrc : srcPoints.getPoints()){
		PointAlignment* aPointAlign = new PointAlignment();
		aPointAlign->setSource(pointSrc);

		minDst = std::numeric_limits<double>::max();

		for (Point* pointTgt : tgtPoints.getPoints()){
			double currDistance = pointSrc->getDistance(pointTgt);
			if ( currDistance < minDst ){
				aPointAlign->setTarget(pointTgt);
				minDst = currDistance;
			} 
		}
		if (aPointAlign->isComplete())
			alignments.push_back(aPointAlign);
	}

	/* Calculate transformation */
	double transformation[3] = {1};
	bool first = true;
	for (PointAlignment* align : alignments){
		double* algTrans;
		if (first) {
			first = false;

			algTrans = align->getTransformation();
			transformation[0] = algTrans[0]; 
			transformation[1] = algTrans[1]; 
			transformation[2] = algTrans[2]; 
		}
		else{
			algTrans = align->getTransformation();
			transformation[0] = (transformation[0]+algTrans[0])/2; 
			transformation[1] = (transformation[1]+algTrans[1])/2; 
			transformation[2] = (transformation[2]+algTrans[2])/2; 
		}
	}
	cout << "TRANSFORMATION: {" << transformation[0] <<"|"<< transformation[1] <<"|"<< transformation[2] <<"}\n";

	return alignments;
};


/************************************************************************************/
std::vector<PointAlignment*> PointCloud::ipcAlignmentWithIndex(PointCloud* target, int threshold){
	//returns a vector of the alignments of the clouds. 

	double minDst;
	std::vector<PointAlignment*> alignments;

	PointSet srcPoints = this->getPointSet();
	PointSet tgtPoints = target->getPointSet();

	/*Find Near neighbor for each point in source.*/
	for (Point* pointSrc : srcPoints.getPoints()){
		PointAlignment* aPointAlign = new PointAlignment();
		aPointAlign->setSource(pointSrc);

		minDst = std::numeric_limits<double>::max();

		int xStart,yStart,zStart;
		xStart = (pointSrc->get_X()-threshold) >=0 ? (pointSrc->get_X()-threshold) : 0;
		yStart = (pointSrc->get_Y()-threshold) >=0 ? (pointSrc->get_Y()-threshold) : 0;
		zStart = (pointSrc->get_Z()-threshold) >=0 ? (pointSrc->get_Z()-threshold) : 0;

		int xEnd,yEnd,zEnd;
		xEnd = (pointSrc->get_X()+threshold) < TREE_SIZE ? (pointSrc->get_X()+threshold) : 0;
		yEnd = (pointSrc->get_Y()+threshold) < TREE_SIZE ? (pointSrc->get_Y()+threshold) : 0;
		zEnd = (pointSrc->get_Z()+threshold) < TREE_SIZE ? (pointSrc->get_Z()+threshold) : 0;

		for (int z= zStart; z < zEnd;z++ ){
			for (int y= yStart; y < yEnd; y++ ){
				for (int x= xStart; x < xEnd;x++ ){
					if (target->getIndexValueAt(x,y,z) == 1 ){	//That point is in the target set
						Point * pointTgt = new Point(x,y,z);
						
						double currDistance = pointSrc->getDistance(pointTgt);
						if ( currDistance < minDst ){
							aPointAlign->setTarget(pointTgt);
							minDst = currDistance;
						} 
					}

				}
			}
		}
 
		if (aPointAlign->isComplete())
			alignments.push_back(aPointAlign);
	}

	/* Calculate transformation */
	double transformation[3] = {1};
	bool first = true;
	for (PointAlignment* align : alignments){

		double* algTrans;
		if (first) {
			first = false;

			algTrans = align->getTransformation();
			transformation[0] = algTrans[0]; 
			transformation[1] = algTrans[1]; 
			transformation[2] = algTrans[2]; 
		}
		else{
			algTrans = align->getTransformation();
			transformation[0] = (transformation[0]+algTrans[0])/2; 
			transformation[1] = (transformation[1]+algTrans[1])/2; 
			transformation[2] = (transformation[2]+algTrans[2])/2; 
		}
	}

	cout << "TRANSFORMATION: {" << transformation[0] <<"|"<< transformation[1] <<"|"<< transformation[2] <<"]\n";

	return alignments;
};

 

/************************************************************************************/
PointSet PointCloud::getPointSet() const{
	return setOfPoints;
}
/************************************************************************************/
std::string PointCloud::getName() const{
	return cloudName;
}


/************************************************************************************/
void PointCloud::setName(string aName){
	this->cloudName = aName;
};
/************************************************************************************/
void PointCloud::addPoint(Point* aPoint){
	if (aPoint != 0){
		this->setOfPoints.addPoint(aPoint);
		/*printf("NEW POINT:%i,%i,%i",aPoint->get_X(),
								aPoint->get_Y(),
								aPoint->get_Z());*/
		this->indexTree->set(	aPoint->get_X(),
								aPoint->get_Y(),
								aPoint->get_Z(),1); 
	}
};

/************************************************************************************/

int PointCloud::getIndexValueAt(int x, int y, int z){
	return this->indexTree->at(x,y,z);
};

/************************************************************************************/

void PointCloud::printIndex(){
	for (int  z= 1; z < TREE_SIZE; z++)
	{
		for (int y = 1; y < TREE_SIZE; y++)
		{
			for (int x = 1; x < TREE_SIZE; x++)
			{
				if (this->indexTree->at(x,y,z) >0 )
					printf("{%i,%i,%i} = %f \n" , x,y,z,this->indexTree->at(x,y,z));
			}
		}
	}
}






















