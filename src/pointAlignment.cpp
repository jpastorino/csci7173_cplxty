#include <stdio.h>      /* printf, scanf, puts, NULL */
#include <stdlib.h>     /* srand, rand */
#include <vector>
#include <string>

#include "./pointAlignment.h"

using namespace pointCloud;

PointAlignment::PointAlignment(){
	this->transformation = new double[3];
	this->transformation[0] = 0; 	this->transformation[1] = 0; 	this->transformation[2] = 0;
};


PointAlignment::~PointAlignment(){
	delete[] transformation;
};

PointAlignment::PointAlignment(Point* aSource, Point* aTarget){
	this->transformation = new double[3];
	this->transformation[0] = 0; 	this->transformation[1] = 0; 	this->transformation[2] = 0;

	this->source = aSource;
	this->target = aTarget;
};
			
Point* PointAlignment::getSource() const{
	return source;
}; 

Point* PointAlignment::getTarget() const{
	return target;
};

void PointAlignment::setSource(Point* aPoint){
	this->source = aPoint;
} ;


void PointAlignment::setTarget(Point* aPoint){
	this->target = aPoint;
} ;


int PointAlignment::isComplete() const{
	if ( (this->getSource() != 0) && (this->getTarget() != 0) )
		return 1;
	else
		return 0;
};


double* PointAlignment::getTransformation(){
	if (! this->isComplete())
		return 0;

	this->transformation[0] = this->target->get_X() - this->source->get_X();
	this->transformation[1] = this->target->get_Y() - this->source->get_Y();
	this->transformation[2] = this->target->get_Z() - this->source->get_Z();

	return this->transformation;
};