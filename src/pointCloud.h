

#ifndef CLASS_POINTCLOUD				
#define CLASS_POINTCLOUD


#include <vector>
#include <iostream>
#include <string>

#include "./point.h"
#include "./pointSet.h"
#include "./pointAlignment.h"
#include "./octree/octree.h"

using namespace std;

namespace pointCloud{


#define TREE_SIZE 4096

	class PointCloud {
	   
		public:
			PointCloud();
			~PointCloud();

			PointCloud(int noOfPoints, string name);
			PointCloud(PointCloud& source);
			PointCloud(PointCloud& source, string cName, int threshold); //Copy with transformation 

			PointSet 		getPointSet() const;
			std::string     getName()     const;

			
			void setName(string aName);
			void addPoint(Point* aPoint);

			int getIndexValueAt(int x, int y, int z);

			void printIndex();


			std::vector<PointAlignment*> ipcAlignment(PointCloud* target);
			std::vector<PointAlignment*> ipcAlignmentWithIndex(PointCloud* target, int threshold);


			friend std::ostream& operator<<(std::ostream& os, const PointCloud& pcd) {  
				os << "This is the point cloud:"<<pcd.getName()<<"{\n";
				os << pcd.getPointSet();
			    os <<"}\n";
			    return os;  
			}  ;  

			
		private:
			PointSet		setOfPoints; 
			string 			cloudName;
			Octree<double>* indexTree;



	};

}

#endif