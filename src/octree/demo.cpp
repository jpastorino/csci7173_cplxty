#include "octree.h"
#include <cstdio>
#include <iostream>


#ifdef WIN32
#include <tchar.h>
int _tmain(int argc, _TCHAR* argv[])
#else
int main(int argc, char* argv[])
#endif
{
	int count = 5;
    Octree<double> o(4096); /* Create 4096x4096x4096 octree containing doubles. */

    //o(1,2,3) = 3.1416;      /* Put pi in (1,2,3). */
    

	//printf("{%i,%i,%i} = %f" ,1,2,3,o.at(1,2,3));

	o.erase(1,2,3);         /* Erase that node. */
	for (int  z= 1; z < count; z++)
	{
		for (int y = 1; y < count; y++)
		{
			for (int x = 1; x < count; x++)
			{
				if ((x+y+z) % 2 ==0)
					o(x,y,z) = x+y+z; 
			}
		}
	}

	for (int  z= 1; z < count; z++)
	{
		for (int y = 1; y < count; y++)
		{
			for (int x = 1; x < count; x++)
			{
				printf("{%i,%i,%i} = %f \n" , x,y,z,o.at(x,y,z));
			}
		}
	}

	Array2D<double> arrz = o.zSlice(3);

	for (int y = 1; y < count; y++)
	{
		for (int x = 1; x < count; x++)
		{
			printf("{%i,%i} = %f \n" , x,y, arrz(y,x) );
		}
	}

 



	return 0;
}

