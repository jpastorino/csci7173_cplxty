//
//  main.cpp
//  pointcloud
//
//  Created by Javier Pastorino on 4/20/17.
//
//

#include <iostream>
#include <iomanip>
#include <cmath>
#include <cstdlib>
#include <stdio.h>
#include <stdlib.h>     /* srand, rand */
#include <time.h>       /* time */
#include <string>
#include <vector>

#include "./pointCloud.h"


using namespace std;
using namespace pointCloud;

/*************************************************************************************/
int readParameters(int argc, char *argv[],int& numberOfPoints, int& thresholdForICP, int& thresholdForGenerateCloud, int& printCloud){
	/*Gets the parameters from input*/
	int isOK = 0;

	if(argc <= 1) 
	{
		cout << "No parameters. Display Help." <<endl;
		cout << "main.exe  numberOfPoints  thresholdForICP  thresholdForGenerateCloud print[0|1]"<<endl;
		isOK = 1;
	}
	else 
	{
		if (argc == 5) {
			cout << "All parameters choosen" <<endl;
			printCloud= atoi(argv[4]);
			thresholdForGenerateCloud= atoi(argv[3]);
			thresholdForICP= atoi(argv[2]);
			numberOfPoints= atoi(argv[1]);
		}
		if (argc == 4) {
			cout << "PrintCloud by defaul (No)" <<endl;
			printCloud=0;
			thresholdForGenerateCloud= atoi(argv[3]);
			thresholdForICP= atoi(argv[2]);
			numberOfPoints= atoi(argv[1]);
		}
		else if (argc == 3) {
			cout << "PrintCloud & thresholdForGenerateCloud by default." <<endl;
			printCloud=0;
			thresholdForGenerateCloud= 10;
			thresholdForICP= atoi(argv[2]);
			numberOfPoints= atoi(argv[1]);
		}
		else if (argc == 2) {
			cout << "PrintCloud, thresholdForICP & thresholdForGenerateCloud by default." <<endl;
			printCloud=0;
			thresholdForGenerateCloud= 10;
			thresholdForICP = 10;
			numberOfPoints = atoi(argv[1]);
		}
		
		//Check values
		if (numberOfPoints <=  0) 
		{
			cout << "Space size must be larger than 0" <<endl;
			isOK = 2;
		}

		if (thresholdForICP <=  0) 
		{
			cout << "thresholdForICP must be larger than 0" <<endl;
			isOK = 3;
		}

		if (thresholdForGenerateCloud <=  0) 
		{
			cout << "thresholdForGenerateCloud must be larger than 0" <<endl;
			isOK = 4;
		}
		if (printCloud !=  0 and printCloud != 1) 
		{
			cout << "Print must be 0 or 1 only." <<endl;
			isOK = 5;
		}

		//display config.
		cout << "Config:"<<endl;
		cout << "----------------------------------"<<endl;
		cout << "numberOfPoints:"<<numberOfPoints<<endl;
		cout << "thresholdForICP:"<<thresholdForICP<<endl;
		cout << "thresholdForGenerateCloud:"<<thresholdForGenerateCloud<<endl;
		cout << "printCloud:"<<printCloud<<endl;
		cout << "----------------------------------"<<endl;
	}
	return isOK;
}


/*************************************************************************************/
int main(int argc, char *argv[]){
	float runtime;
	int numberOfPoints,thresholdForICP,thresholdForGenerateCloud,printCloud=0;
	vector<PointAlignment*> icpAlign;

	srand (time(NULL));  //Random initializer

	cout<<"*****************************************************"<<endl;
	cout << "Running Both Optimized ICP Versions." <<endl;
	cout<<"*****************************************************"<<endl;

	int paramValid = readParameters(argc,argv,numberOfPoints,thresholdForICP,thresholdForGenerateCloud,printCloud);
	if (  paramValid > 0 ) return paramValid; 


	string pcd1Name("Source");
	string pcd2Name("Target");
	PointCloud* pcd1 = new PointCloud(numberOfPoints, pcd1Name);
	PointCloud* pcd2 = new PointCloud(*pcd1, pcd2Name, thresholdForGenerateCloud);


	if (printCloud == 1 ){

		cout<< "Source Point Cloud"<<endl;
		cout<<"----------------------------------"<<endl;
		cout << *pcd1  <<"\n\n\n\n\n\n\n\n";
		cout<< "Target Point Cloud"<<endl;
		cout<<"----------------------------------"<<endl;
		cout << *pcd2  <<"\n";
	}
	 

	/******* Run ICP INDEXED **************/

	cout<< "\n\nStarting ICP INDEXED..."<<endl;
	cout<<"__________________________________"<<endl;
	runtime = clock()/(float)CLOCKS_PER_SEC;
		
	icpAlign = pcd1->ipcAlignmentWithIndex(pcd2,thresholdForICP);

	runtime = clock()/(float)CLOCKS_PER_SEC - runtime;	

	cout<< "["<<numberOfPoints<<" points] ICP INDEXEDran in " << setiosflags(ios::fixed) << setprecision(2) << runtime << " seconds"<<endl; 


	/******* Run ICP INDEXED **************/

	cout<< "\n\nStarting ICP INDEXED Optimized..."<<endl;
	cout<<"__________________________________"<<endl;
	runtime = clock()/(float)CLOCKS_PER_SEC;
		
	icpAlign = pcd1->ipcAlignmentWithIndexOptimized(pcd2,thresholdForICP);

	runtime = clock()/(float)CLOCKS_PER_SEC - runtime;	

	cout<< "["<<numberOfPoints<<" points] ICP INDEXEDran in " << setiosflags(ios::fixed) << setprecision(2) << runtime << " seconds"<<endl; 
	


	delete pcd1;	 
	delete pcd2;		
	return 0;

}

