


#ifndef CLASS_POINTSET		
#define CLASS_POINTSET


#include <vector>
#include <iostream>
#include <string>

#include "./point.h"

using namespace std;

namespace pointCloud{

	class PointSet {
	   
		public:
			PointSet();

			vector<Point *> 	getPoints() const;
			int 					addPoint(Point * aPoint);

			friend ostream& operator<<(ostream& os, const PointSet& pSet) {  
				os << "PointSet[\n";
				int i =0;
				for(Point* point : pSet.getPoints()) {
			        os << *point ;
			        i++;
			        if (i % 10 == 0 ) os<<'\n';
			    }
			    os<<"]\n";
			    return os;  
			}  ;  

			
		private:
			vector<Point * > 	points; 
	};

}

#endif