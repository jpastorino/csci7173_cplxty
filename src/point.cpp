

#include <cmath>
#include "./point.h"

using namespace pointCloud; 

Point::Point(long x,long y, long z){
	this->x=x;
	this->y=y;
	this->z=z;
};


double Point::getDistance(Point* aPoint){/*Returns euclidean distances between two points.*/

	return sqrt(pow((this->x)-(aPoint->get_X()),2.0)+
				pow((this->y)-(aPoint->get_Y()),2.0)+
				pow((this->z)-(aPoint->get_Z()),2.0)
				);
};

long Point::get_X() const {return this->x;};
long Point::get_Y() const {return this->y;};
long Point::get_Z() const {return this->z;};


