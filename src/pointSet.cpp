



#include <stdio.h>      /* printf, scanf, puts, NULL */
#include <stdlib.h>     /* srand, rand */
#include <vector>
#include <string>

#include "./pointSet.h"

using namespace pointCloud;


PointSet::PointSet(){
	/*Default constructor for pointset.*/
};

vector<Point *> PointSet::getPoints() const{
	return points;
}


int PointSet::addPoint(Point * aPoint){
	this->points.push_back(aPoint);

	return 0;
};

