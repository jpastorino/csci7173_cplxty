
#ifndef CLASS_POINT				
#define CLASS_POINT

#include <iostream>
using namespace std;

namespace pointCloud{

	class Point{

		public:
		
			Point(long x,long y, long z);

			long get_X() const;
			long get_Y() const;
			long get_Z() const;

			double getDistance(Point* aPoint);

			friend std::ostream& operator<<(std::ostream& os, const Point& aPoint){
				//os<<"Point:";
				os << "(" << aPoint.get_X() << "," << aPoint.get_Y() << "," << aPoint.get_Z() <<")"; 
				return os;
			}; 
		
		private:
			long x;
			long y;
			long z;

	};

}
#endif
