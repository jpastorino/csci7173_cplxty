#ifndef CLASS_POINTALIGNMENT
#define CLASS_POINTALIGNMENT


#include <iostream>

#include "./point.h"

/*This class represents that the source point is corresponding with the target*/

using namespace std;

namespace pointCloud{

	class PointAlignment {
	   
		public:
			PointAlignment();
			~PointAlignment();
			PointAlignment(Point* aSource, Point* aTarget);
			
			Point* getSource() const; 
			Point* getTarget() const;

			void setSource(Point* aPoint)  ; 
			void setTarget(Point* aPoint)  ; 

			int isComplete() const;
			double* getTransformation();

			friend std::ostream& operator<<(std::ostream& os,  PointAlignment& pa) {  
				if ( pa.isComplete()){
					double* t = pa.getTransformation();
					os << "PointAlignment["<<*(pa.getSource())<<','<<*(pa.getTarget()) << "] Transf:["<< t[0] <<"|"<< t[1] <<"|"<< t[2] <<"]\n";
				}
				else
					if ((pa.getSource() == 0))
						os << "PointAlignment[null src,"<<*(pa.getTarget()) << "]\n";
					else
						os << "PointAlignment["<<*(pa.getSource())<<",null tgt]\n";
			    return os;  
			}  ;  
			
		private:
			Point* source; 
			Point* target; 
			double* transformation;
	};

}

#endif