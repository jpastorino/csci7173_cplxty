#!/bin/bash


OUTPUT="./report.txt"
POINT_LIST="100 1000 5000 10000 20000 50000 100000 200000"

for points in $POINT_LIST
do
	echo "Running app for $points points" | tee -a $OUTPUT
	./main.exe $points 10 10 0  | tee -a $OUTPUT
done
