# CSCI7173 Computational Complexity and Algorithms  
## Final course project
__Goal__: ICP complexity evaluation of the algorithm with and without the use of
data indexes. 

- __Author__: Javier Pastorino (www.jpastorino.com)
- __Date__: Spring 2017